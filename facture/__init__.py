import gorilla
import copy

import pandas
import facture
import inspect

settings = gorilla.Settings(allow_hit=True, store_hit=True)


functions = inspect.getmembers(pandas, inspect.isfunction)
functions_df = inspect.getmembers(pandas.DataFrame, inspect.isfunction)

def patch(module, function_name, function_substitute):
    gorilla.apply(gorilla.Patch(module, function_name, function_substitute, settings=settings))

def patch_read():
    read_functions = []
    for f in functions:
        if 'read_' in f[0]:
            read_functions.append(f)

    def substitute_func(function_name):
        def read(*args, **kwargs):
            print(f"{function_name} is Called !!!! ==>", args)
            original = gorilla.get_original_attribute(pandas, function_name)
            return original(*args, **kwargs)

        return read

    for function_name, func in read_functions:
        substituted_func = substitute_func(function_name)
        patch(pandas, function_name, substituted_func)



def patch_save():
    save_functions = []
    for f in functions_df:
        if 'to_' in f[0]:
            save_functions.append(f)

    def substitute_func(function_name):
        def save(*args, **kwargs):

            if args.__len__()>=2:
                my_path = args[1]
                print(f" {function_name} is Called !!!! ==>", my_path)

            original = gorilla.get_original_attribute(pandas.DataFrame, function_name)
            return original(*args, **kwargs)

        return save

    for function_name, func in save_functions:
        substituted_func = substitute_func(function_name)
        patch(pandas.DataFrame, function_name, substituted_func)




patch_read()

patch_save()
