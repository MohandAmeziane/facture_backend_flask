import random

from flask import Flask, jsonify, request
import os
from backend.utils.util import read_json, take_img_only, get_api_key, img2json, IMG_EXTENSION
from faker import Faker




DATA_PATH = os.path.expanduser("~/img")
TAGS_PATH = os.path.expanduser("~/tags/tags.json")

app = Flask(__name__)

@app.route('/api/test')
def test():
    return "hello"

@app.route('/api/files')
def list_files():
    files = os.listdir(DATA_PATH)
    print(files)
    final_data_list = []
    for file_name in files:
        img_path = f"{DATA_PATH}/{file_name}"
        final_data_list.append({
            "name": file_name,
            "path": img_path,
            "isExists": os.path.exists(img_path.replace(".png", ".json"))

        })

    return jsonify(list(filter(take_img_only, final_data_list)))


@app.route('/api/json/<id>')
def get_json(id):
    print("get_json")
    file_name = str(id).replace(IMG_EXTENSION, "")
    print("file_name", file_name)
    img_path = f"{DATA_PATH}/{file_name}{IMG_EXTENSION}"
    json_path = f"{DATA_PATH}/{file_name}.json"
    is_exists = os.path.exists(json_path)
    if (not is_exists):
        print("img_path", img_path)
        img2json(img_path, file_name, DATA_PATH)

    result = read_json(json_path)
    return jsonify(result)


@app.route('/api/tags')
def get_tags():
    return jsonify(read_json(TAGS_PATH))


@app.route('/api/fake', methods=["POST"])
def fake():
    print("fake", request.data)

    with open("C:\\Users\\ADBI_101\\Desktop\\BIGAPPS\\dev\\facture\\backend\\data/test.json", 'w') as json_file:
        json_file.write(request.data.decode('utf-8'))
    return ""


app.run()
if __name__ == '__main__':
    fake = Faker('fr_FR')
    print('hello', random.randint(-10, 10))
    #fake.date().format("%d-%m-%y")
