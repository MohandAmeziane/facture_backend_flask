from backend.utils.util import read_json
from backend.utils.data_generator import DataGenerator, DateDataFaker, TotalDataFaker


def test_generating_fake_json():
    #given
    INVOICE_PATH = "C:\\Users\\ADBI_101\\Desktop\\BIGAPPS\\dev\\facture_backend_flask\\backend\\data\\test.json"
    invoice_data = read_json(INVOICE_PATH)


    print("invoice_data", invoice_data)
    print("invoice_data", invoice_data)
    print("invoice_data", invoice_data)

    fakers = {
        'DATE_FACT': [DateDataFaker("DATE_FACT","%d/%m/%Y"), DateDataFaker("DATE_FACT","%d-%m-%Y")],
        'TOTAL': [TotalDataFaker("TOTAL", "1"), TotalDataFaker("TOTAL", "2")],
    }



    #when
    result = DataGenerator(fakers).generate(invoice_data)
    print("result", result)

    #then



if __name__ == '__main__':
    test_generating_fake_json()