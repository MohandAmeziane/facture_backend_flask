import cv2
import requests
import io
import time
import json

IMG_EXTENSION = '.png'
URL = 'https://api8.ocr.space/parse/image'
API_KEYS = ['f4be3aae1088957', '5a64d478-9c89-43d8-88e3-c65de9999580']


def take_img_only(file):
    return 'name' in file and file['name'].endswith(IMG_EXTENSION)


def read_json(json_path):
    with open(json_path, 'r+') as fd:
        data = json.load(fd)
        return data


def get_api_key():
    index = int(time.time() % 2)
    return API_KEYS[index]


def img2json(img_path, file_name, folder):
    img = cv2.imread(img_path)
    height, width, _ = img.shape
    _, compressed = cv2.imencode('.png', img, [1, 90])
    bytes = io.BytesIO(compressed)
    print("prepare request")
    result = requests.post(URL,
                           files={"fact140001-1.png": bytes},
                           data={"apikey": get_api_key(), 'isOverlayRequired': 'true'}
                           )

    with open(folder + '/' + file_name + '.json', 'w') as f:
        f.write(result.text)
