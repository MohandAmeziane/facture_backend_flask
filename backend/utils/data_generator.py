import copy
import random

import pandas as pd
import abc


from faker import Faker
fake = Faker('fr_FR')

DELTA_Left = random.randint(-10, 10)
DELTA_Top = random.randint(-10, 10)


class DataFaker(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, label):
        self.label = label

    @abc.abstractmethod
    def generate(self):
        return ""


class DateDataFaker(DataFaker):
    def __init__(self, label, format):
        super().__init__(label)
        self.format = format

    def generate(self):
        return fake.date(self.format)


class TotalDataFaker(DataFaker):
    def __init__(self, label, format):
        super().__init__(label)
        self.format = format

    def generate(self):
        return fake.pyfloat( right_digits=2, positive=True)

class DataGenerator(object):
    def __init__(self, fakers):
        self.fakers = fakers
        self.cols = fakers.keys()
        products = []
        for key in self.cols:
            products.append(fakers[key])

        index = pd.MultiIndex.from_product(products, names=self.cols)
        df = pd.DataFrame(index=index).reset_index()
        records = list(df.to_records(index=False))
        self.fake_invoice_data = records

    def generate(self, invoice_data_nodes):

        generated_invoices = []

        non_labelized_nodes = list(filter(lambda n: 'label' not in n, invoice_data_nodes))
        labelized_nodes = list(filter(lambda n: 'label' in n, invoice_data_nodes))

        invoice_index = 0
        for fake_invoice in self.fake_invoice_data:

            final_invoice_data = []
            for labelized_node in labelized_nodes:
                new_node = copy.copy(labelized_node)
                label = new_node['label']
                print("self.cols",list(self.cols))
                index = list(self.cols).index(label)

                print('Current Label ', label, "fake_invoice", fake_invoice[index].generate())

                new_node['WordText'] = fake_invoice[index].generate()
                new_node['Left'] = labelized_node['Left'] + DELTA_Left
                new_node['Top'] = labelized_node['Top'] + DELTA_Top
                new_node['Height'] = round((labelized_node['Height'] * str(new_node['WordText']).__len__()) / str(labelized_node['WordText']).__len__())

                print("new_node['WordText']", new_node['WordText'])
                print("new_node['WordText']", new_node['Left'], "labelized_node['Left']", labelized_node['Left'])
                print("new_node['WordText']", new_node['Top'], "labelized_node['Top'] ", labelized_node['Top'] )
                print("new_node['WordText']", new_node['Height'], "labelized_node['Height']", labelized_node['Height'])


                final_invoice_data.append(new_node)

            # add non labelized node
            for non_labelized_node in non_labelized_nodes:
                final_invoice_data.append(non_labelized_node)
            print(f"Fake invoice {invoice_index}==>", final_invoice_data)
            invoice_index += 1

            generated_invoices.append(fake_invoice)

        return generated_invoices
