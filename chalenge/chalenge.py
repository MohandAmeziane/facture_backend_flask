import pandas as pd
from pyspark.sql import SparkSession, DataFrame
from sklearn import preprocessing

def listToString(list):
    binary_str = ""
    for elt in list:
        binary_str = binary_str + str(elt)
    return binary_str



TRAIN_PATH = "../chalenge/train.csv"


if __name__ == '__main__':
    spark = SparkSession.builder \
        .config(key="spark.python.worker.reuse", value=True) \
        .getOrCreate()
    df: DataFrame = spark.read.csv(TRAIN_PATH, header=True, inferSchema=True)
    products_df = df[df.columns[8:20]]
    products_df.show()

#    enc = preprocessing.OrdinalEncoder()













